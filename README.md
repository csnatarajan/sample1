# Sample 1 #
This example computes the PCA (BLAS, LAPACK) for dimensionality reduction and support vectors (Dlib) for classification. The input data file is in ascii (terrible idea) and contains dimensions (features) as columns and samples as rows. Once compiled <executable> --help should provide sample usage.

### Dependencies ###

1. [Dlib](http://dlib.net/)
2. [Trilinos](http://trilinos.org/)
3. [Intel MKL](https://software.intel.com/en-us/intel-mkl)